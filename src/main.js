import { createApp } from 'vue'
import App from './App.vue'
import components from './components/UI/index.js'
import router from './router/router'
import directives from './directives'
import store from './store'

const app = createApp(App)

for (const component of components) {
  app.component(component.name, component)
}

for (const directive of directives) {
  app.directive(directive.name, directive)
}

app
  .use(router)
  .use(store)
  .mount('#app')
