export default {
    state() {
        return {
            posts: [],
            loading: false,
            selectedSort: '',
            searchQuery: '',
            currentPage: 1,
            pageSize: 10,
            totalPages: 0,
            sortOptions: [
                {value: 'title', name: 'Sort by title'},
                {value: 'body', name: 'Sort by body'},
                {value: 'id', name: 'Sort by id'},
            ]
        }
    },
    mutations: {
        setPosts(state, posts) {
            state.posts = posts
        },
        setLoading(state, loading) {
            state.loading = loading
        },
        setSelectedSort(state, selectedSort) {
            state.selectedSort = selectedSort
        },
        setSearchQuery(state, searchQuery) {
            state.searchQuery = searchQuery
        },
        setCurrentPage(state, currentPage) {
            state.currentPage = currentPage
        },
        setTotalPages(state, totalPages) {
            state.totalPages = totalPages
        },
    },
    getters: {
        sortedPosts(state) {
            return [...state.posts].sort((a,b) => {
              const aProperty = a[state.selectedSort]
              const bProperty = b[state.selectedSort]
    
              return typeof aProperty === 'number' ? aProperty - bProperty : aProperty?.localeCompare(bProperty)
            })
          },
        sortedAndFilteredPosts(state, getters) {
           return getters.sortedPosts.filter(post => post.title.includes(state.searchQuery))
        },
    },
    actions: {
        async fetchPosts({state, commit}) {
            commit('setLoading', true)
    
            try {
              const queryParams = new URLSearchParams({_page: state.currentPage, _limit: state.pageSize}).toString()
              const response = await fetch('https://jsonplaceholder.typicode.com/posts?' + queryParams, {method: 'GET'})
    
              if(!response.ok) throw new Error('Cannot get posts')
              
              commit('setTotalPages', Math.ceil(Object.fromEntries(response.headers)['x-total-count'] / state.pageSize))
              commit('setPosts', await response.json())
            } catch (error) {
              console.error(error)
            } finally {
              commit('setLoading', false)
            }
          },
          async loadMorePosts({state, commit}) {
            commit('setCurrentPage', state.currentPage + 1)
    
            try {
              const queryParams = new URLSearchParams({_page: this.currentPage, _limit: this.pageSize}).toString()
              const response = await fetch('https://jsonplaceholder.typicode.com/posts?' + queryParams, {method: 'GET'})
    
              if(!response.ok) throw new Error('Cannot get posts')
              
              commit('setTotalPages', Math.ceil(Object.fromEntries(response.headers)['x-total-count'] / this.pageSize))
              const newPosts = await response.json()
              commit('setPosts', [...state.posts, ...newPosts])
            } catch (error) {
              console.error(error)
            }
          },
    },
    namespaced: true,
}