import { createStore } from "vuex";
import PostsModule from "./PostsModule";

export default createStore({
    modules: {
        posts: PostsModule,
    }
})