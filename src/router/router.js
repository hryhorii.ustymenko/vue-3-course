import Main from '../pages/Main.vue'
import PostPage from '../pages/PostPage.vue'
import PostPageWithStore from '../pages/PostPageWithStore.vue'
import PostDetails from '../pages/PostDetails.vue'
import About from '../pages/About.vue'
import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        path: '/',
        component: Main
    },
    {
        path: '/posts',
        component: PostPage
    },
    {
        path: '/about',
        component: About
    },
    {
        path: '/posts/:id',
        component: PostDetails
    },
    {
        path: '/store',
        component: PostPageWithStore
    }
]

const router = createRouter({routes, history: createWebHistory(import.meta.env.BASE_URL)})

export default router